﻿using Microsoft.Extensions.Logging;
using SwingFitApp.CQRS.CheckForUpdates.Dto;
using System;
using System.Net.Http;
using System.Text.Json;
using System.Threading.Tasks;

namespace SwingFitApp.CQRS.CheckForUpdates
{
    internal class CheckForUpdatesQuery
    {
        private readonly string _serverUrl;
        private readonly ILogger<CheckForUpdatesQuery> _logger;

        public CheckForUpdatesQuery(string serverUrl, ILogger<CheckForUpdatesQuery> logger)
        {
            _serverUrl = serverUrl;
            _logger = logger;
        }

        public async Task<string> ExecuteAsync(string currentVersion)
        {
            string path = null;
            using (var httpClient = new HttpClient())
            {
                HttpResponseMessage response = null;
                try
                {
                    response = await httpClient.GetAsync($"{_serverUrl}/settings");
                    response.EnsureSuccessStatusCode();
                    string responseBody = await response.Content.ReadAsStringAsync();
                    var settings = JsonSerializer.Deserialize<CheckForUpdatesResponseDto>(responseBody);

                    if (new Version(settings.desktop_version).CompareTo(new Version(currentVersion)) > 0)
                    {
                        path = settings.desktop_path;
                    }
                }
                catch (Exception ex)
                {
                    _logger.LogError("Get settings error: {message}", ex.Message);
                }
                finally
                {
                    response?.Dispose();
                }

            }
            return path;
        }
    }
}

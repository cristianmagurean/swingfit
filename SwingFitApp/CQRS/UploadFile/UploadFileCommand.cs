﻿using Microsoft.Extensions.Logging;
using SwingFitApp.Constants;
using SwingFitApp.Helpers;
using System;
using System.ComponentModel;
using System.IO;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;

namespace SwingFitApp.CQRS.UploadFile
{
    public class UploadFileCommand
    {
        private readonly string _serverUrl;
        private readonly int _retryCount;
        private readonly int _retryFrequency;
        private readonly ILogger<UploadFileCommand> _logger;

        public UploadFileCommand(string serverUrl, int retryCount, int retryFrequency, ILogger<UploadFileCommand> logger)
        {
            _serverUrl = serverUrl;
            _retryCount = retryCount;
            _retryFrequency = retryFrequency;
            _logger = logger;
        }

        public async Task<ServerOperationStatus> ExecuteAsync(BackgroundWorker backgroundWorker, string filePath, string token)
        {
            using var httpClient = new HttpClient();
            httpClient.BaseAddress = new Uri(_serverUrl);
            httpClient.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("*/*"));
            httpClient.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", token);

            var requestContent = new MultipartFormDataContent();
            try
            {
                _logger.LogDebug("Attempt to read file {filePath}", filePath);
                var byteArray = FileHelper.WriteSafeReadByteArray(filePath);
                _logger.LogDebug("File content {filePath} read successfully", filePath);
                var fileContent = new ByteArrayContent(byteArray);
                fileContent.Headers.ContentType = new MediaTypeHeaderValue("multipart/form-data");
                requestContent.Add(fileContent, "file", Path.GetFileName(filePath));
            }
            catch (Exception ex)
            {
                _logger.LogError("File {filePath} read attempt with error: {message}", filePath, ex.Message);
                return ServerOperationStatus.Error;
            }

            int retryCount = 0;
            HttpResponseMessage response = null;
            while ((retryCount <= _retryCount) && (!backgroundWorker.CancellationPending))
            {
                try
                {
                    _logger.LogDebug("Attempt to upload file {filePath}", filePath);
                    response = await httpClient.PostAsync("upload_gpcap", requestContent);
                    retryCount++;
                    if (response.IsSuccessStatusCode)
                    {
                        _logger.LogInformation("File {filePath} uploaded with success", filePath);
                        return ServerOperationStatus.Success;
                    }
                    else
                    {
                        _logger.LogError("File {filePath} uploaded with error: {status} {reason} on attempt no {attempt}", filePath, response.StatusCode, response.ReasonPhrase, retryCount);
                        if (response.StatusCode == HttpStatusCode.Unauthorized)
                        {
                            return ServerOperationStatus.Unauthorized;
                        }
                    }
                }
                catch (Exception ex)
                {
                    _logger.LogError("File {filePath} uploaded with error: {message} on attempt no {attempt}", filePath, ex.Message, retryCount);
                }
                finally
                {
                    response?.Dispose();
                }

                if (_retryFrequency > 0)
                {
                    await Task.Delay(_retryFrequency * 1000);
                }
            }
            return ServerOperationStatus.Error;
        }
    }
}

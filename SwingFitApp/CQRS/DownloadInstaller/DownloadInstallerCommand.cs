﻿using Microsoft.Extensions.Logging;
using System;
using System.IO;
using System.Net.Http;
using System.Threading.Tasks;

namespace SwingFitApp.CQRS.DownloadInstaller
{
    public class DownloadInstallerCommand
    {
        private readonly ILogger<DownloadInstallerCommand> _logger;

        public DownloadInstallerCommand(ILogger<DownloadInstallerCommand> logger)
        {
            _logger = logger;
        }

        public async Task ExecuteAsync(string pathUrl, string localPath)
        {
            try
            {
                File.Delete(localPath);
                Task<byte[]> downloadTask = GetFileContentAsync(pathUrl);
                downloadTask.Wait();
                var fileBytes = await downloadTask;
                if (downloadTask.IsCompletedSuccessfully)
                {
                    File.WriteAllBytes(localPath, fileBytes);
                }
            }
            catch (Exception ex)
            {
                _logger.LogError("Download {pathUrl} with error: {message}", pathUrl, ex.Message);
            }
        }

        private static async Task<byte[]> GetFileContentAsync(string pathUrl)
        {
            using var httpClient = new HttpClient();
            httpClient.Timeout = TimeSpan.FromMinutes(5);
            var fileContent = await httpClient.GetByteArrayAsync(pathUrl);
            return fileContent;
        }
    }
}

﻿using Microsoft.Extensions.Logging;
using SwingFitApp.Configuration;
using SwingFitApp.CQRS.CheckForUpdates;
using SwingFitApp.CQRS.DownloadInstaller;
using SwingFitApp.Helpers;
using SwingFitApp.Token;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Windows.Forms;

namespace SwingFitApp.UI
{
    public partial class DashboardFrm : Form
    {
        private readonly ILogger<DashboardFrm> _logger;
        private readonly WorkerOptions _options;
        private readonly ILoggerFactory _loggerFactory;
        private readonly TokenManager _tokenManager;
        private string _downloadUrl;
        private string _localPath;

        public DashboardFrm(WorkerOptions options, ILoggerFactory loggerFactory)
        {
            InitializeComponent();
            StartPosition = FormStartPosition.Manual;
            Location = new Point(Screen.PrimaryScreen.WorkingArea.Width - this.Width, Screen.PrimaryScreen.WorkingArea.Height - this.Height);

            _options = options;
            _loggerFactory = loggerFactory;
            _tokenManager = new TokenManager(_loggerFactory);
            _logger = loggerFactory.CreateLogger<DashboardFrm>();
        }

        private void DashboardFrm_Load(object sender, EventArgs e)
        {
            SetStyle(ControlStyles.SupportsTransparentBackColor, true);
            BackColor = Color.Transparent;

            backgroundWorkerCheckForUpdates.RunWorkerAsync();

            RefreshLogs();

            contextMenuStripGearLogs.Items.Add("Download", null, btnDownload_Click);
            contextMenuStripGearLogs.Items.Add("Open folder", null, btnOpenFolder_Click);
        }

        private void backgroundWorkerCheckForUpdates_DoWork(object sender, System.ComponentModel.DoWorkEventArgs e)
        {
            var query = new CheckForUpdatesQuery(WorkerOptions.ServerUrl, _loggerFactory.CreateLogger<CheckForUpdatesQuery>());
            var version = Assembly.GetExecutingAssembly().GetName().Version;
            e.Result = query.ExecuteAsync($"{version.Major}.{version.Minor}.{version.Build}").Result;
        }

        private void backgroundWorkerCheckForUpdates_RunWorkerCompleted(object sender, System.ComponentModel.RunWorkerCompletedEventArgs e)
        {
            if (e.Error == null && !e.Cancelled)
            {
                if (e.Result is not null)
                {
                    lblUpgradeStatus.Text = "New version is available.";
                    btnUpgrade.Enabled = true;
                    _downloadUrl = e.Result as string;
                }
                else
                {
                    lblUpgradeStatus.Text = "The installed version is up to date.";
                    btnUpgrade.Enabled = false;
                }
            }
        }

        private void btnUpgrade_Click(object sender, System.EventArgs e)
        {
            _localPath = Path.Combine(Path.GetDirectoryName(Environment.GetFolderPath(Environment.SpecialFolder.Personal)), "Downloads", "SwingFitAppInstaller.msi");
            MessageBox.Show("The installer will start automatically after the download will be completed!", "Information");
            backgroundWorkerDownload.RunWorkerAsync();
        }

        private void backgroundWorkerDownload_DoWork(object sender, System.ComponentModel.DoWorkEventArgs e)
        {
            var command = new DownloadInstallerCommand(_loggerFactory.CreateLogger<DownloadInstallerCommand>());
            _ = command.ExecuteAsync(_downloadUrl, _localPath);
        }

        private void backgroundWorkerDownload_RunWorkerCompleted(object sender, System.ComponentModel.RunWorkerCompletedEventArgs e)
        {
            if (e.Error == null && !e.Cancelled)
            {
                StartInstaller(_localPath);
                Application.Exit();
            }
        }

        private void StartInstaller(string localPath)
        {
            try
            {
                Process.Start("msiexec", $"/i {localPath}");
            }
            catch (Exception ex)
            {
                _logger.LogError("Process {localPath} launched with error: {message}", localPath, ex.Message);
            }
        }

        private void LoadFiles(string folder, ListBox listBox)
        {
            listBox.Items.Clear();
            var directoryInfo = new DirectoryInfo(folder);
            ICollection<string> logFiles = directoryInfo.GetFiles("*.log", SearchOption.TopDirectoryOnly)
                .AsEnumerable()
                .OrderByDescending(x => x.CreationTimeUtc)
                .Select(x => x.FullName)
                .ToList();

            foreach (string filePath in logFiles)
            {
                listBox.Items.Add(Path.GetFileName(filePath));
            }
            if (listBox.Items.Count > 0)
            {
                listBox.SelectedIndex = 0;
            }
        }

        private void LoadActivities(string file)
        {
            dataGridActivityLines.Rows.Clear();
            var lines = FileHelper.WriteSafeReadLines(Path.Combine(WorkerOptions.LogsFolder, file));
            foreach (var line in lines)
            {
                int rowIdx = dataGridActivityLines.Rows.Add();
                DataGridViewRow dataGridViewRow = dataGridActivityLines.Rows[rowIdx];
                dataGridViewRow.Cells["Activity"].Value = line;
                if (line.Contains("[ERR]"))
                {
                    dataGridViewRow.DefaultCellStyle.ForeColor = Color.Red;
                }
            }
        }

        private void LoadSyncedFiles(string file)
        {
            dataGridSyncedFiles.Rows.Clear();
            var lines = FileHelper.ReadAllLines(Path.Combine($"{WorkerOptions.LogsFolder}/Info", file));
            foreach (var line in lines)
            {
                int rowIdx = dataGridSyncedFiles.Rows.Add();
                DataGridViewRow dataGridViewRow = dataGridSyncedFiles.Rows[rowIdx];
                dataGridViewRow.Cells["FileName"].Value = line[42..].Replace(" uploaded with success", string.Empty);
                dataGridViewRow.Cells["Timestamp"].Value = line[..19];
            }
        }

        private void listBoxActivityLogs_SelectedIndexChanged(object sender, EventArgs e)
        {
            LoadActivities(listBoxActivityLogs.SelectedItem.ToString());
        }

        private void listBoxSyncLogs_SelectedIndexChanged(object sender, EventArgs e)
        {
            LoadSyncedFiles(listBoxSyncLogs.SelectedItem.ToString());
        }

        private void btnRefresh_Click(object sender, EventArgs e)
        {
            RefreshLogs();
        }

        private void RefreshLogs()
        {
            LoadFiles(WorkerOptions.LogsFolder, listBoxActivityLogs);
            if (listBoxActivityLogs.Items.Count > 0)
            {
                listBoxActivityLogs.SelectedIndex = 0;
                LoadActivities(listBoxActivityLogs.Items[0].ToString());
            }

            LoadFiles($"{WorkerOptions.LogsFolder}/Info", listBoxSyncLogs);
            if (listBoxSyncLogs.Items.Count > 0)
            {
                listBoxSyncLogs.SelectedIndex = 0;
                LoadSyncedFiles(listBoxSyncLogs.Items[0].ToString());
            }
        }

        private void btnActions_Click(object sender, EventArgs e)
        {
            contextMenuStripActions.Items.Clear();
            if (!_tokenManager.IsTokenValid())
            {
                contextMenuStripActions.Items.Add("Login", null, btnLogin_Click);
            }
            else
            {
                contextMenuStripActions.Items.Add("Sign-out", null, btnSignOut_Click);
            }
            contextMenuStripActions.Items.Add("View Sync Issues", null, btnSync_Click);
            contextMenuStripActions.Items.Add("Help", null, btnHelp_Click);
            contextMenuStripActions.Items.Add("Settings", null, btnSettings_Click);
            contextMenuStripActions.Items.Add("Exit", null, btnExit_Click);
            contextMenuStripActions.Show(btnActions, new Point(0, btnActions.Height));
        }

        private void btnLogin_Click(object sender, EventArgs e)
        {
            _logger.LogInformation("User action: Attempt sign in");

            var frmLogin = new LoginFrm(WorkerOptions.ServerUrl, _tokenManager, _loggerFactory);
            DialogResult dialogResult = frmLogin.ShowDialog();
            if (dialogResult == DialogResult.OK)
            {
                _logger.LogInformation("User action: Sign in succesfull");
            }
        }

        private void btnSignOut_Click(object sender, EventArgs e)
        {
            _logger.LogInformation("User action: Sign out");
            _tokenManager.DeleteToken();
        }

        private void btnSync_Click(object sender, EventArgs e)
        {
            _logger.LogInformation("User action: Open Sync logs form");
            if (_tokenManager.IsTokenValid())
            {
                var frmSync = new SyncFrm(_tokenManager.GetAuthToken(), _options, _loggerFactory);
                frmSync.ShowDialog();
            }
            else
            {
                MessageBox.Show("Please sign in to continue", "Warning");
            }
        }

        private void btnSettings_Click(object sender, EventArgs e)
        {
            _logger.LogInformation("User action: Open Settings form");
            var frmSettings = new SettingsFrm(_options, _tokenManager, _loggerFactory);
            DialogResult dialogResult = frmSettings.ShowDialog();
            if (dialogResult == DialogResult.OK)
            {
                _logger.LogInformation("Settings updated");
                //ResetBackgroundWorker();
            }
        }

        private void btnHelp_Click(object sender, EventArgs e)
        {
            _logger.LogInformation("User action: Open Help");
            Process.Start("notepad.exe", Path.Combine($"{Application.StartupPath}", "Help", "help.txt"));
        }

        private void btnExit_Click(object sender, EventArgs e)
        {
            _logger.LogInformation("User action: Exit application");
            Application.Exit();
        }

        private void btnDownload_Click(object sender, EventArgs e)
        {
            _logger.LogInformation("User action: Download Gears log file");
            var filePath = dataGridSyncedFiles.CurrentRow.Cells[0].Value as string;
            if (File.Exists(filePath))
            {
                var fileName = Path.GetFileName(filePath);
                var saveFileDialog = new SaveFileDialog
                {
                    Filter = "Gears file|*.gpcap",
                    Title = "Save an Gears Log File",
                    FileName = fileName
                };
                if (saveFileDialog.ShowDialog() == DialogResult.OK)
                {
                    File.Copy(filePath, saveFileDialog.FileName, true);
                };
            }
            else
            {
                MessageBox.Show($"The path {filePath} does not exists!", "Informatiomn");
            }
        }

        private void btnOpenFolder_Click(object sender, EventArgs e)
        {
            _logger.LogInformation("User action: Open folder with Gears log files");
            var filePath = dataGridSyncedFiles.CurrentRow.Cells[0].Value as string;
            var path = Path.GetDirectoryName(filePath);
            FolderHelper.OpenFolder(path);
        }
    }
}

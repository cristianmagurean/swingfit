﻿using Microsoft.Extensions.Logging;
using SwingFitApp.CQRS.Login;
using System;
using System.IO;
using System.Text.Json;
using System.Threading.Tasks;

namespace SwingFitApp.Token
{
    public class TokenManager
    {
        const string FileName = "token.json";

        private string authToken;
        private string refreshToken;
        private readonly string _pathFile;
        private readonly ILoggerFactory _loggerFactory;

        public TokenManager(ILoggerFactory loggerFactory)
        {
            var path = Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData), System.Reflection.Assembly.GetExecutingAssembly().GetName().Name);
            if (!Directory.Exists(path))
            {
                Directory.CreateDirectory(path);
            }
            _pathFile = Path.Combine(path, FileName);
            _loggerFactory = loggerFactory;
        }

        public bool IsTokenValid()
        {
            if (authToken != null)
            {
                return true;
            }
            if (File.Exists(_pathFile))
            {
                string json = File.ReadAllText(_pathFile);
                var tokenFile = JsonSerializer.Deserialize<Token>(json);
                if (tokenFile == null)
                {
                    return false;
                }
                authToken = tokenFile.AuthToken;
                return tokenFile.AuthToken?.Length > 0;
            }
            return false;
        }

        public string GetAuthToken()
        {
            if (authToken == null)
            {
                string json = File.ReadAllText(_pathFile);
                authToken = JsonSerializer.Deserialize<Token>(json).AuthToken;
            }
            return authToken;
        }

        public string GetRefreshToken()
        {
            if (refreshToken == null)
            {
                string json = File.ReadAllText(_pathFile);
                refreshToken = JsonSerializer.Deserialize<Token>(json).RefreshToken;
            }
            return refreshToken;
        }

        public void StoreToken(Token token)
        {
            if (token != null)
            {
                string json = JsonSerializer.Serialize<Token>(token);
                File.WriteAllText(_pathFile, json);
                authToken = token.AuthToken;
                refreshToken = token.RefreshToken;
            }
        }

        public void DeleteToken()
        {
            authToken = null;
            refreshToken = null;
            File.Delete(_pathFile);
        }

        public async Task<Token> GenerateToken(string serverUrl, string email, string key)
        {
            var loginCommand = new LoginCommand(serverUrl, _loggerFactory.CreateLogger<LoginCommand>());
            return await loginCommand.ExecuteAsync(email, key);
        }
    }
}

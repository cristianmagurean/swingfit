﻿using System.Reflection;

namespace SwingFitApp.Helpers
{
    public static class VersionHelper
    {
        public static string GetCurrentVersion()
        {
            var version = Assembly.GetExecutingAssembly().GetName().Version;
            return $"{version.Major}.{version.Minor}.{version.Build}";
        }
    }
}

﻿using System.Collections.Generic;
using System.IO;

namespace SwingFitApp.Helpers;

internal static class FileHelper
{
    public static IEnumerable<string> WriteSafeReadLines(string path)
    {
        List<string> lines = new();
        using var fileStream = new FileStream(path, FileMode.Open, FileAccess.Read, FileShare.ReadWrite);
        using var sr = new StreamReader(fileStream);
        while (!sr.EndOfStream)
        {
            lines.Add(sr.ReadLine());
        }
        return lines;
    }

    public static byte[] WriteSafeReadByteArray(string path)
    {
        using var fileStream = new FileStream(path, FileMode.Open, FileAccess.Read, FileShare.ReadWrite);
        byte[] result = new byte[fileStream.Length];
        fileStream.Read(result, 0, (int)fileStream.Length);
        return result;
    }

    public static IEnumerable<string> ReadAllLines(string path)
    {
        List<string> lines = new();
        try
        {
            using var fileStream = new FileStream(path, FileMode.Open, FileAccess.Read, FileShare.ReadWrite);
            using var sr = new StreamReader(fileStream);
            while (!sr.EndOfStream)
            {
                lines.Add(sr.ReadLine());
            }
            return lines;
        }
        catch (IOException)
        {
            return lines;
        }
    }

    public static string ReadLastLine(string path)
    {
        string line = string.Empty;
        using var fileStream = new FileStream(path, FileMode.Open, FileAccess.Read, FileShare.ReadWrite);
        using var sr = new StreamReader(fileStream);
        while (!sr.EndOfStream)
        {
            line = sr.ReadLine();
        }
        return line;
    }
}

﻿namespace SwingFitApp.CQRS.Login.Dto
{
    internal class LoginResponseDto
    {
        public string status { get; set;}
        public string message { get; set; }
        public string auth_token { get; set; }
        public string refresh_token { get; set; }
    }
}

﻿using Microsoft.Extensions.Logging;
using SwingFitApp.Configuration;
using SwingFitApp.Constants;
using SwingFitApp.CQRS.UploadFile;
using SwingFitApp.Helpers;
using SwingFitApp.LogManagement;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.IO;
using System.Linq;
using System.Windows.Forms;

namespace SwingFitApp.UI
{
    public partial class SyncFrm : Form
    {
        private readonly string _token;
        private readonly WorkerOptions _options;
        private readonly ILoggerFactory _loggerFactory;
        private readonly ProcessPendingLogs _processPendingLogs;

        public SyncFrm(string token, WorkerOptions options, ILoggerFactory loggerFactory)
        {
            InitializeComponent();
            _token = token;
            _options = options;
            _loggerFactory = loggerFactory;

            _processPendingLogs = new ProcessPendingLogs(WorkerOptions.ServerUrl,
               WorkerOptions.GearsFolder,
               WorkerOptions.LogsFolder,
               WorkerOptions.RetryCount,
               WorkerOptions.RetryFrequency,
               "*.gpcap",
               _loggerFactory.CreateLogger<UploadFileCommand>());

            RefreshData();
        }

        private void RefreshData()
        {
            lblLastSync.Text = $"Last sync at: {GetLastSyncTimeStamp($"{WorkerOptions.LogsFolder}/Info")}";
            var pendingFiles = _processPendingLogs.GetPendingFiles((int)numericUpDownDays.Value);
            LoadData(pendingFiles);
        }

        private void LoadData(ICollection<string> files)
        {
            dataGridViewFiles.Rows.Clear();
            foreach (var file in files)
            {
                int rowIdx = dataGridViewFiles.Rows.Add();
                DataGridViewRow dataGridViewRow = dataGridViewFiles.Rows[rowIdx];
                dataGridViewRow.Cells["PendingFile"].Value = file;
                var fileInfo = new FileInfo(file);
                dataGridViewRow.Cells["Timestamp"].Value = fileInfo.CreationTime;
            }
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void btnSync_Click(object sender, EventArgs e)
        {
            backgroundWorker.RunWorkerAsync();
            RefreshData();
        }

        private async void backgroundWorker_DoWork(object sender, DoWorkEventArgs e)
        {
            var status = await _processPendingLogs.Execute(backgroundWorker, _token, (int)numericUpDownDays.Value);
            if (status == ServerOperationStatus.Unauthorized)
            {
                MessageBox.Show("Please sign in with authorized user credentials to continue", "Warning");
            }
        }

        private string GetLastSyncTimeStamp(string folder)
        {
            string lastSyncTimeStamp = string.Empty;
            if (Directory.Exists(folder))
            {
                var latestLogFile = new DirectoryInfo(folder).GetFiles().OrderByDescending(o => o.LastWriteTime).FirstOrDefault();
                if (latestLogFile != null)
                {
                    string lastLogLine = FileHelper.ReadLastLine(latestLogFile.FullName);
                    lastSyncTimeStamp = lastLogLine[..19];
                }
            }
            return lastSyncTimeStamp;
        }

        private void btnRefresh_Click(object sender, EventArgs e)
        {
            RefreshData();
        }
    }
}

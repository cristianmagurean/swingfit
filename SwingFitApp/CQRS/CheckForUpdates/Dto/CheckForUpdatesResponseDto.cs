﻿namespace SwingFitApp.CQRS.CheckForUpdates.Dto
{
    internal class CheckForUpdatesResponseDto
    {
        public string desktop_version { get; set;}
        public string desktop_path { get; set; }       
    }
}

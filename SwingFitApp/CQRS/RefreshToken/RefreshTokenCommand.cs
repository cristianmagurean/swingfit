﻿using Microsoft.Extensions.Logging;
using SwingFitApp.CQRS.Login.Dto;
using System;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text.Json;
using System.Threading.Tasks;

namespace SwingFitApp.CQRS.RefreshToken
{
    internal class RefreshTokenCommand
    {
        private readonly string _serverUrl;
        private readonly ILogger<RefreshTokenCommand> _logger;

        public RefreshTokenCommand(string serverUrl, ILogger<RefreshTokenCommand> logger)
        {
            _serverUrl = serverUrl;
            _logger = logger;
        }

        public async Task<Token.Token> ExecuteAsync(string refreshToken)
        {
            using var httpClient = new HttpClient();
            httpClient.BaseAddress = new Uri(_serverUrl);
            httpClient.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", refreshToken);
            HttpResponseMessage response = null;
            try
            {
                response = await httpClient.PostAsync("refresh_token", null);
                response.EnsureSuccessStatusCode();
                var responseContent = await response.Content.ReadAsStringAsync().ConfigureAwait(false);
                var refreshTokenResponse = JsonSerializer.Deserialize<LoginResponseDto>(responseContent);
                return new Token.Token
                {
                    AuthToken = refreshTokenResponse.auth_token,
                    RefreshToken = refreshTokenResponse.refresh_token
                };
            }
            catch (Exception ex)
            {
                _logger.LogError("Refresh token attempt with error: {message}", ex.Message);
            }
            finally
            {
                response?.Dispose();
            }
            return null;
        }
    }
}

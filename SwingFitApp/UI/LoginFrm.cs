﻿using Microsoft.Extensions.Logging;
using SwingFitApp.Token;
using System;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace SwingFitApp.UI
{
    public partial class LoginFrm : Form
    {
        private readonly string _serverUrl;
        private readonly TokenManager _tokenManager;
        private readonly ILogger _logger;

        public LoginFrm(string serverUrl, TokenManager tokenManager, ILoggerFactory loggerFactory)
        {
            InitializeComponent();
            CancelButton = btnCancel;
            _serverUrl = serverUrl;
            _tokenManager = tokenManager;
            _logger = loggerFactory.CreateLogger<LoginFrm>();
            checkBoxRememberKey.Checked = true;
        }

        private void btnOK_Click(object sender, EventArgs e)
        {
            lblAccessDenied.Visible = false;
            if (ValidateChildren())
            {
                errorProvider.Clear();
                var token = Task.Run(async () => await _tokenManager.GenerateToken(_serverUrl, txtEmail.Text.Trim(), txtKey.Text.Trim())).Result;
                if (token != null)
                {
                    if (checkBoxRememberKey.Checked)
                    {
                        _tokenManager.StoreToken(token);
                    }
                    DialogResult = DialogResult.OK;
                    Close();
                }
                else
                {
                    _logger.LogInformation("Authentication token is not valid");
                    lblAccessDenied.Visible = true;
                }
            }
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            DialogResult = DialogResult.Cancel;
            Close();
        }

        private static bool IsValidEmail(string email)
        {
            try
            {
                _ = new System.Net.Mail.MailAddress(email);
                return true;
            }
            catch
            {
                return false;
            }
        }

        private void txtKey_Validating(object sender, System.ComponentModel.CancelEventArgs e)
        {
            if (txtKey.TextLength == 0)
            {
                errorProvider.SetError(txtKey, "Please enter a key");
                e.Cancel = true;
            }
        }

        private void txtEmail_Validating(object sender, System.ComponentModel.CancelEventArgs e)
        {
            if (!IsValidEmail(txtEmail.Text))
            {
                errorProvider.SetError(txtEmail, "Please enter a valid email address");
                e.Cancel = true;
            }
        }
    }
}

﻿
namespace SwingFitApp.UI
{
    partial class SettingsFrm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(SettingsFrm));
            this.lblServerURL = new System.Windows.Forms.Label();
            this.btnCancel = new System.Windows.Forms.Button();
            this.btnOK = new System.Windows.Forms.Button();
            this.lblGearsFilesFolder = new System.Windows.Forms.Label();
            this.lblRetryFrequency = new System.Windows.Forms.Label();
            this.lblRetryCount = new System.Windows.Forms.Label();
            this.txtServerURL = new System.Windows.Forms.TextBox();
            this.txtGearsFolder = new System.Windows.Forms.TextBox();
            this.txtLogsFolder = new System.Windows.Forms.TextBox();
            this.lblLogsFolder = new System.Windows.Forms.Label();
            this.numericUpDownRetryCount = new System.Windows.Forms.NumericUpDown();
            this.numericUpDownRetryFrequency = new System.Windows.Forms.NumericUpDown();
            this.folderBrowserDialog = new System.Windows.Forms.FolderBrowserDialog();
            this.errorProvider = new System.Windows.Forms.ErrorProvider(this.components);
            this.btnSelectGearsFolder = new System.Windows.Forms.Button();
            this.btnSelectLogsFolder = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownRetryCount)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownRetryFrequency)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider)).BeginInit();
            this.SuspendLayout();
            // 
            // lblServerURL
            // 
            this.lblServerURL.AutoSize = true;
            this.lblServerURL.Location = new System.Drawing.Point(78, 49);
            this.lblServerURL.Name = "lblServerURL";
            this.lblServerURL.Size = new System.Drawing.Size(161, 41);
            this.lblServerURL.TabIndex = 0;
            this.lblServerURL.Text = "Server URL";
            // 
            // btnCancel
            // 
            this.btnCancel.Location = new System.Drawing.Point(1071, 367);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.Size = new System.Drawing.Size(188, 58);
            this.btnCancel.TabIndex = 1;
            this.btnCancel.Text = "Cancel";
            this.btnCancel.UseVisualStyleBackColor = true;
            this.btnCancel.Click += new System.EventHandler(this.btnCancel_Click);
            // 
            // btnOK
            // 
            this.btnOK.Location = new System.Drawing.Point(835, 367);
            this.btnOK.Name = "btnOK";
            this.btnOK.Size = new System.Drawing.Size(188, 58);
            this.btnOK.TabIndex = 2;
            this.btnOK.Text = "Save";
            this.btnOK.UseVisualStyleBackColor = true;
            this.btnOK.Click += new System.EventHandler(this.btnOK_Click);
            // 
            // lblGearsFilesFolder
            // 
            this.lblGearsFilesFolder.AutoSize = true;
            this.lblGearsFilesFolder.Location = new System.Drawing.Point(78, 128);
            this.lblGearsFilesFolder.Name = "lblGearsFilesFolder";
            this.lblGearsFilesFolder.Size = new System.Drawing.Size(185, 41);
            this.lblGearsFilesFolder.TabIndex = 4;
            this.lblGearsFilesFolder.Text = "Gears Folder";
            // 
            // lblRetryFrequency
            // 
            this.lblRetryFrequency.AutoSize = true;
            this.lblRetryFrequency.Location = new System.Drawing.Point(658, 286);
            this.lblRetryFrequency.Name = "lblRetryFrequency";
            this.lblRetryFrequency.Size = new System.Drawing.Size(356, 41);
            this.lblRetryFrequency.TabIndex = 6;
            this.lblRetryFrequency.Text = "Retry Frequency(minutes)";
            // 
            // lblRetryCount
            // 
            this.lblRetryCount.AutoSize = true;
            this.lblRetryCount.Location = new System.Drawing.Point(78, 286);
            this.lblRetryCount.Name = "lblRetryCount";
            this.lblRetryCount.Size = new System.Drawing.Size(175, 41);
            this.lblRetryCount.TabIndex = 8;
            this.lblRetryCount.Text = "Retry Count";
            // 
            // txtServerURL
            // 
            this.txtServerURL.Location = new System.Drawing.Point(335, 48);
            this.txtServerURL.Name = "txtServerURL";
            this.txtServerURL.Size = new System.Drawing.Size(924, 47);
            this.txtServerURL.TabIndex = 9;
            this.txtServerURL.Validating += new System.ComponentModel.CancelEventHandler(this.txtServerURL_Validating);
            // 
            // txtGearsFolder
            // 
            this.txtGearsFolder.Location = new System.Drawing.Point(335, 125);
            this.txtGearsFolder.Name = "txtGearsFolder";
            this.txtGearsFolder.Size = new System.Drawing.Size(803, 47);
            this.txtGearsFolder.TabIndex = 10;
            this.txtGearsFolder.Validating += new System.ComponentModel.CancelEventHandler(this.txtGearsFolder_Validating);
            // 
            // txtLogsFolder
            // 
            this.txtLogsFolder.Location = new System.Drawing.Point(335, 202);
            this.txtLogsFolder.Name = "txtLogsFolder";
            this.txtLogsFolder.Size = new System.Drawing.Size(803, 47);
            this.txtLogsFolder.TabIndex = 12;
            this.txtLogsFolder.Validating += new System.ComponentModel.CancelEventHandler(this.txtLogsFolder_Validating);
            // 
            // lblLogsFolder
            // 
            this.lblLogsFolder.AutoSize = true;
            this.lblLogsFolder.Location = new System.Drawing.Point(78, 207);
            this.lblLogsFolder.Name = "lblLogsFolder";
            this.lblLogsFolder.Size = new System.Drawing.Size(173, 41);
            this.lblLogsFolder.TabIndex = 11;
            this.lblLogsFolder.Text = "Logs Folder";
            // 
            // numericUpDownRetryCount
            // 
            this.numericUpDownRetryCount.Location = new System.Drawing.Point(335, 280);
            this.numericUpDownRetryCount.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.numericUpDownRetryCount.Name = "numericUpDownRetryCount";
            this.numericUpDownRetryCount.Size = new System.Drawing.Size(174, 47);
            this.numericUpDownRetryCount.TabIndex = 13;
            this.numericUpDownRetryCount.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.numericUpDownRetryCount.Validating += new System.ComponentModel.CancelEventHandler(this.numericUpDownRetryCount_Validating);
            // 
            // numericUpDownRetryFrequency
            // 
            this.numericUpDownRetryFrequency.Location = new System.Drawing.Point(1085, 280);
            this.numericUpDownRetryFrequency.Maximum = new decimal(new int[] {
            120,
            0,
            0,
            0});
            this.numericUpDownRetryFrequency.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.numericUpDownRetryFrequency.Name = "numericUpDownRetryFrequency";
            this.numericUpDownRetryFrequency.Size = new System.Drawing.Size(174, 47);
            this.numericUpDownRetryFrequency.TabIndex = 14;
            this.numericUpDownRetryFrequency.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.numericUpDownRetryFrequency.Validating += new System.ComponentModel.CancelEventHandler(this.numericUpDownRetryFrequency_Validating);
            // 
            // errorProvider
            // 
            this.errorProvider.ContainerControl = this;
            // 
            // btnSelectGearsFolder
            // 
            this.btnSelectGearsFolder.AccessibleDescription = "Select Gears Folder";
            this.btnSelectGearsFolder.CausesValidation = false;
            this.btnSelectGearsFolder.Location = new System.Drawing.Point(1193, 126);
            this.btnSelectGearsFolder.Name = "btnSelectGearsFolder";
            this.btnSelectGearsFolder.Size = new System.Drawing.Size(66, 49);
            this.btnSelectGearsFolder.TabIndex = 16;
            this.btnSelectGearsFolder.Text = "...";
            this.btnSelectGearsFolder.UseVisualStyleBackColor = true;
            this.btnSelectGearsFolder.Click += new System.EventHandler(this.btnSelectGearsFolder_Click);
            // 
            // btnSelectLogsFolder
            // 
            this.btnSelectLogsFolder.AccessibleDescription = "Select Logs Folder";
            this.btnSelectLogsFolder.CausesValidation = false;
            this.btnSelectLogsFolder.Location = new System.Drawing.Point(1193, 206);
            this.btnSelectLogsFolder.Name = "btnSelectLogsFolder";
            this.btnSelectLogsFolder.Size = new System.Drawing.Size(66, 49);
            this.btnSelectLogsFolder.TabIndex = 17;
            this.btnSelectLogsFolder.Text = "...";
            this.btnSelectLogsFolder.UseVisualStyleBackColor = true;
            this.btnSelectLogsFolder.Click += new System.EventHandler(this.btnSelectLogsFolder_Click);
            // 
            // SettingsFrm
            // 
            this.AcceptButton = this.btnOK;
            this.AutoScaleDimensions = new System.Drawing.SizeF(17F, 41F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1341, 465);
            this.Controls.Add(this.btnSelectLogsFolder);
            this.Controls.Add(this.btnSelectGearsFolder);
            this.Controls.Add(this.numericUpDownRetryFrequency);
            this.Controls.Add(this.numericUpDownRetryCount);
            this.Controls.Add(this.txtLogsFolder);
            this.Controls.Add(this.lblLogsFolder);
            this.Controls.Add(this.txtGearsFolder);
            this.Controls.Add(this.txtServerURL);
            this.Controls.Add(this.lblRetryCount);
            this.Controls.Add(this.lblRetryFrequency);
            this.Controls.Add(this.lblGearsFilesFolder);
            this.Controls.Add(this.btnOK);
            this.Controls.Add(this.btnCancel);
            this.Controls.Add(this.lblServerURL);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.Fixed3D;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "SettingsFrm";
            this.Text = "Settings";
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownRetryCount)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownRetryFrequency)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label lblServerURL;
        private System.Windows.Forms.Button btnCancel;
        private System.Windows.Forms.Button btnOK;
        private System.Windows.Forms.Label lblGearsFilesFolder;
        private System.Windows.Forms.Label lblRetryFrequency;
        private System.Windows.Forms.Label lblRetryCount;
        private System.Windows.Forms.TextBox txtServerURL;
        private System.Windows.Forms.TextBox txtGearsFolder;
        private System.Windows.Forms.TextBox txtLogsFolder;
        private System.Windows.Forms.Label lblLogsFolder;
        private System.Windows.Forms.NumericUpDown numericUpDownRetryCount;
        private System.Windows.Forms.NumericUpDown numericUpDownRetryFrequency;
        private System.Windows.Forms.FolderBrowserDialog folderBrowserDialog;
        private System.Windows.Forms.ErrorProvider errorProvider;
        private System.Windows.Forms.Button btnSelectLogsFolder;
        private System.Windows.Forms.Button btnSelectGearsFolder;
    }
}
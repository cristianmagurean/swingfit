﻿namespace SwingFitApp.Token
{
    public class Token
    {
        public string AuthToken { get; set; }
        public string RefreshToken { get; set; }       
    }
}

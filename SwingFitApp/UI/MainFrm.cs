﻿using Microsoft.Extensions.Logging;
using SwingFitApp.Configuration;
using SwingFitApp.Constants;
using SwingFitApp.CQRS.ArchiveLogs;
using SwingFitApp.CQRS.CheckServerResponse;
using SwingFitApp.CQRS.RefreshToken;
using SwingFitApp.CQRS.UploadFile;
using SwingFitApp.CQRS.UploadLogs;
using SwingFitApp.LogManagement;
using SwingFitApp.Token;
using System;
using System.IO;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace SwingFitApp.UI
{
    public partial class MainFrm : Form
    {
        private readonly ILogger<MainFrm> _logger;
        private readonly WorkerOptions _options;
        private readonly ILoggerFactory _loggerFactory;
        private readonly TokenManager _tokenManager;
        private readonly FileWatcher _fileWatcher;

        public MainFrm(WorkerOptions options, ILoggerFactory loggerFactory)
        {
            _options = options;
            _loggerFactory = loggerFactory;
            _logger = loggerFactory.CreateLogger<MainFrm>();

            InitializeComponent();

            _logger.LogInformation("Starting application");
            _logger.LogInformation("Application started at: {time}", DateTimeOffset.Now);
            _logger.LogInformation("Application uses serverUrl: {ServerUrl}", WorkerOptions.ServerUrl);

            _tokenManager = new TokenManager(_loggerFactory);
            _fileWatcher = new FileWatcher("*.gpcap", _loggerFactory.CreateLogger<FileWatcher>());
        }

        private void MainFrm_Load(object sender, EventArgs e)
        {
            if (!CheckSettings())
            {
                Application.Exit();
                return;
            }

            if (!CheckServer())
            {
                Application.Exit();
                return;
            }

            if (!CheckCredentials())
            {
                Application.Exit();
                return;
            }

            StartMonitoring(_fileWatcher);

            StartSplashScreenTimer();

            UploadLogs();
            StartUploadLogsTimer();

            ArchiveLogs();
            StartArchiveTimer();
        }

        #region startup
        private bool CheckSettings()
        {
            _logger.LogInformation("Validating settings");
            if (!WorkerOptions.ValidateSettings())
            {
                ViewSettings();
                if (!WorkerOptions.ValidateSettings())
                {
                    _logger.LogInformation("Please check the application settings!");
                    return false;
                }
            }
            _logger.LogInformation("The settings are valid");
            return true;
        }

        private bool CheckCredentials()
        {
            _logger.LogInformation("Checking if token exists");
            if (!_tokenManager.IsTokenValid())
            {
                _logger.LogInformation("User action: Attempt sign in");
                var frmLogin = new LoginFrm(WorkerOptions.ServerUrl, _tokenManager, _loggerFactory);
                DialogResult dialogResult = frmLogin.ShowDialog();
                if (dialogResult == DialogResult.OK)
                {
                    _logger.LogInformation("User action: Sign in succesfull");
                }
                else
                {
                    _logger.LogInformation("User action: Sign in failure");
                    return false;
                }
            }
            _logger.LogInformation("Token exists");
            return true;
        }

        private bool CheckServer()
        {
            _logger.LogInformation("Checking the server is online!");
            var query = new CheckServerResponseQuery(WorkerOptions.ServerUrl, _loggerFactory.CreateLogger<CheckServerResponseQuery>());
            var result = Task.Run(async () => await query.ExecuteAsync()).Result;
            if (!result)
            {
                _logger.LogInformation("The server is not online!");
                return false;
            }
            _logger.LogInformation("The server is online!");
            return true;
        }
        #endregion

        #region splash scren
        private void StartSplashScreenTimer()
        {
            WindowState = FormWindowState.Normal;
            var timer = new Timer
            {
                Interval = 3 * 1000 // 3 sec
            };
            timer.Start();
            timer.Tick += SplashScreenTimer_Tick;
        }

        private void SplashScreenTimer_Tick(object sender, EventArgs e)
        {
            var timer = (Timer)sender;
            timer.Stop();
            WindowState = FormWindowState.Minimized;
            ActivateDashboard();
        }
        #endregion

        #region archive
        private void StartArchiveTimer()
        {
            var timer = new Timer
            {
                Interval = 60 * 60 * 1000 //hourly
            };
            timer.Start();
            timer.Tick += ArchiveTimer_Tick;
        }

        private void ArchiveTimer_Tick(object sender, EventArgs e)
        {
            ArchiveLogs();
        }

        private void ArchiveLogs()
        {
            var archiveFolder = Path.Combine(WorkerOptions.LogsFolder, "archives", DateTime.Today.Year.ToString(), DateTime.Today.Month.ToString());
            var command = new ArchiveLogsCommand(WorkerOptions.LogsFolder, archiveFolder, "*.log", 30, _loggerFactory.CreateLogger<ArchiveLogsCommand>());
            command.Execute();
        }

        #endregion

        #region upload logs to server
        private void StartUploadLogsTimer()
        {
            var timer = new Timer
            {
                Interval = 60 * 60 * 1000 //hourly
            };
            timer.Start();
            timer.Tick += UploadLogsTimer_Tick;
        }

        private void UploadLogsTimer_Tick(object sender, EventArgs e)
        {
            UploadLogs();
        }

        private void UploadLogs()
        {
            Task.Run(async () => await UploadLogsAsync());
        }

        private async Task UploadLogsAsync()
        {
            var command = new UploadLogsCommand(WorkerOptions.ServerUrl, _loggerFactory.CreateLogger<UploadLogsCommand>());
            var filePath = Path.Combine(WorkerOptions.LogsFolder, $"log{DateTime.Today:yyyyMMdd}.log");
            if (File.Exists(filePath))
            {
                if (await RefreshToken())
                {
                    await command.ExecuteAsync(filePath, _tokenManager.GetAuthToken());
                }
                else
                {
                    _tokenManager.DeleteToken();
                    MessageBox.Show("Please log in to continue", "Warning");
                }
            }
        }
        #endregion              

        private async Task<bool> RefreshToken()
        {
            var command = new RefreshTokenCommand(WorkerOptions.ServerUrl, _loggerFactory.CreateLogger<RefreshTokenCommand>());
            var refreshToken = _tokenManager.GetRefreshToken();
            var token = await command.ExecuteAsync(refreshToken);
            if (token != null)
            {
                _tokenManager.StoreToken(token);
                return true;
            }
            return false;
        }

        private void ViewSettings()
        {
            _logger.LogInformation("User action: Open Settings form");
            var frmSettings = new SettingsFrm(_options, _tokenManager, _loggerFactory);
            DialogResult dialogResult = frmSettings.ShowDialog();
            if (dialogResult == DialogResult.OK)
            {
                _logger.LogInformation("Settings updated");
                ResetBackgroundWorker();
            }
        }

        protected void Exit_EventHandler(Object sender, System.EventArgs e)
        {
            _logger.LogInformation("User action: Exit application");
            backgroundWorker.CancelAsync();
            Close();
        }

        private void MainFrm_Resize(object sender, EventArgs e)
        {
            if (WindowState == FormWindowState.Minimized)
            {
                Hide();
                notifyIcon.Visible = true;
            }
        }

        private void StartMonitoring(FileWatcher fileWatcher)
        {
            if (Directory.Exists(WorkerOptions.GearsFolder))
            {
                _logger.LogInformation("Initialize File Watcher at: {time}", DateTimeOffset.Now);
                fileWatcher.MonitorDirectory(WorkerOptions.GearsFolder);
                fileWatcher.FileCreated += FileCreatedEvent_Handler;
                _logger.LogInformation("Services started");
            }
        }

        private async Task FileCreatedEvent_Handler(Object sender, FileCreatedEventArgs e)
        {
            _logger.LogInformation("The file creation event received for {file}.", e.FilePath);
            var fileUploader = new UploadFileCommand(WorkerOptions.ServerUrl, WorkerOptions.RetryCount, WorkerOptions.RetryFrequency, _loggerFactory.CreateLogger<UploadFileCommand>());

            System.Threading.Thread.Sleep(500);

            var status = await fileUploader.ExecuteAsync(backgroundWorker, e.FilePath, _tokenManager.GetAuthToken());
            if (status == ServerOperationStatus.Unauthorized)
            {
                if (await RefreshToken())
                {
                    await fileUploader.ExecuteAsync(backgroundWorker, e.FilePath, _tokenManager.GetAuthToken());
                }
                else
                {
                    _tokenManager.DeleteToken();
                    MessageBox.Show("Please sign in with authorized user credentials to continue", "Warning");
                }
            }
        }

        private void ResetBackgroundWorker()
        {
            _logger.LogInformation("Cancel background worker");
            backgroundWorker.CancelAsync();
            while (backgroundWorker.IsBusy)
            {
                Task.Delay(1000);
            }
            _logger.LogInformation("Start background worker");
            backgroundWorker.RunWorkerAsync();
        }

        private void NotifyIcon_MouseClick(object sender, MouseEventArgs e)
        {
            ActivateDashboard();
        }

        private void ActivateDashboard()
        {
            _logger.LogInformation("User action: Open dashboard form");
            foreach (Form form in Application.OpenForms)
            {
                if (form.GetType() == typeof(DashboardFrm))
                {
                    form.Activate();
                    return;
                }
            }
            var frmDashboard = new DashboardFrm(_options, _loggerFactory);
            frmDashboard.Show();
        }
    }
}

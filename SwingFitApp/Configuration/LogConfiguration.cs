using Serilog;
using Serilog.Core;
using Serilog.Filters;
using SwingFitApp.CQRS.UploadFile;
using SwingFitApp.LogManagement;
using SwingFitApp.Properties;

namespace SwingFitApp.Configuration
{
    public static class LogConfiguration
    {
        public static Logger ConfigureSerilog(string logsFolder)
        {
            return new LoggerConfiguration()
                .MinimumLevel.Debug()
                .Enrich.WithEnvironmentUserName()
                .Enrich.WithMachineName()
                .Enrich.WithEnvironmentName()
                .Enrich.FromLogContext()
                .WriteTo.Console()

                //log all details
                .WriteTo.File($"{logsFolder}/log.log", rollingInterval: RollingInterval.Day)

                //log only info about file upload
                .WriteTo.Logger(lc => lc
                    .Enrich.FromLogContext()
                    .Filter.ByIncludingOnly(Matching.FromSource<UploadFileCommand>())
                    .Filter.ByIncludingOnly(le => le.Level.Equals(Serilog.Events.LogEventLevel.Information))
                    .WriteTo.File($"{logsFolder}/Info/log.log", rollingInterval: RollingInterval.Day))

                 //log all errors
                 .WriteTo.Logger(lc => lc
                    .Filter.ByIncludingOnly(le => le.Level.Equals(Serilog.Events.LogEventLevel.Error))
                    .WriteTo.File($"{logsFolder}/Errors/log.log", rollingInterval: RollingInterval.Day))
            .CreateLogger();
        }
    }
}
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                     
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using Serilog;
using SwingFitApp.Configuration;
using SwingFitApp.UI;
using System;
using System.Collections.Generic;
using System.IO;
using System.Reflection;
using System.Windows.Forms;

namespace SwingFitApp;

static class Program
{
    /// <summary>
    ///  The main entry point for the application.
    /// </summary>
    [STAThread]
    static void Main()
    {
        if (!SingleInstance.Start("0f03c714-b597-4c17-a351-62f35535599a"))
        {
            MessageBox.Show("Application is already running.");
            return;
        }

        var defaultLogsFolder = Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData),
                       Assembly.GetExecutingAssembly().GetName().Name);

        try
        {
            Application.SetHighDpiMode(HighDpiMode.SystemAware);
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);

            WorkerOptions options = new();

            ///Generate Host Builder and Register the Services for DI
            var builder = new HostBuilder()
               .ConfigureServices((hostingContext, services) =>
               {
                   if (!Directory.Exists(WorkerOptions.LogsFolder))
                   {
                       WorkerOptions.LogsFolder = defaultLogsFolder;
                       Directory.CreateDirectory(WorkerOptions.LogsFolder);
                   }
                   if (!Directory.Exists($"{WorkerOptions.LogsFolder}/Info"))
                   {
                       Directory.CreateDirectory($"{WorkerOptions.LogsFolder}/Info");
                   }
                   if (!Directory.Exists($"{WorkerOptions.LogsFolder}/Errors"))
                   {
                       Directory.CreateDirectory($"{WorkerOptions.LogsFolder}/Errors");
                   }
                   var serilogLogger = LogConfiguration.ConfigureSerilog(WorkerOptions.LogsFolder);
                   services.AddLogging(x =>
                   {
                       x.AddSerilog(logger: serilogLogger, dispose: true);
                   });
               });

            var host = builder.Build();

            using (var serviceScope = host.Services.CreateScope())
            {
                var services = serviceScope.ServiceProvider;
                var loggerFactory = services.GetRequiredService<ILoggerFactory>();
                Application.Run(new MainFrm(options, loggerFactory));
            }
        }
        catch (Exception ex)
        {
            Directory.CreateDirectory(defaultLogsFolder);
            File.AppendAllLines(Path.Combine(defaultLogsFolder, "initializationLog.txt"), new List<string>() { ex.Message, ex.StackTrace });
        }
        finally
        {
            // mark single instance as closed
            SingleInstance.Stop();
        }
    }
}

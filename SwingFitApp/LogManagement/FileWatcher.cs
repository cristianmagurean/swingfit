﻿using Microsoft.Extensions.Logging;
using SwingFitApp.Properties;
using System;
using System.IO;
using System.Threading.Tasks;

namespace SwingFitApp.LogManagement
{
    public class FileWatcher
    {
        private readonly string _logFilter;
        private readonly ILogger<FileWatcher> _logger;
        FileSystemWatcher _watcher;
        public AsyncEventHandler<FileCreatedEventArgs> FileCreated;

        public FileWatcher(string logFilter, ILogger<FileWatcher> logger)
        {
            _watcher = new();
            _logFilter = logFilter;
            _logger = logger;
        }
       
        public delegate Task AsyncEventHandler<FileCreatedEventArgs>(object sender, FileCreatedEventArgs e);

        public void MonitorDirectory(string path)
        {
            _logger.LogInformation("Monitor directory intialized: {0}", path);          
            _watcher.Path = path;
            _watcher.IncludeSubdirectories = true;
            _watcher.NotifyFilter = NotifyFilters.LastAccess | NotifyFilters.LastWrite
                                   | NotifyFilters.FileName | NotifyFilters.DirectoryName;
            _watcher.Filter = _logFilter;
            _watcher.Created += FileSystemWatcher_Created;
            _watcher.EnableRaisingEvents = true;
        }

        private void FileSystemWatcher_Created(object source, FileSystemEventArgs e)
        {
            _logger.LogInformation("File created file watcher event raised: {0}", e.Name);
            FileCreated?.Invoke(this, new FileCreatedEventArgs() { FilePath = e.FullPath });
        }
    }

    public class FileCreatedEventArgs : EventArgs
    {
        public string FilePath { get; set; }        
    }    
}

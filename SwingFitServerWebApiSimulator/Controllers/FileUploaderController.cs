﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;

namespace SwingFitServerWebApiSimulator.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class FileUploaderController : ControllerBase
    {
        private readonly ILogger<FileUploaderController> _logger;

        public FileUploaderController(ILogger<FileUploaderController> logger)
        {
            _logger = logger;
        }

        [HttpPost("upload_gpcap")]
        public async Task UploadFile()
        {
            try
            {
                using (var ms = new MemoryStream(2048))
                {
                    await Request.Body.CopyToAsync(ms);
                    var fileContent = ms.ToArray();

                    var path = "D:\\UploadFiles";

                    if (!Directory.Exists(path))
                    {
                        Directory.CreateDirectory(path);
                    }

                    var pathFile = Path.Combine(path, $"Log.txt");

                    System.IO.File.WriteAllBytes(pathFile, fileContent);
                }
            }
            catch(Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
        }        
    }
}

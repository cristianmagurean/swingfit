﻿using System.Diagnostics;
using System.IO;

namespace SwingFitApp.Helpers
{
    internal static class FolderHelper
    {
        public static void OpenFolder(string folderPath)
        {
            if (Directory.Exists(folderPath))
            {
                ProcessStartInfo startInfo = new()
                {
                    Arguments = folderPath,
                    FileName = "explorer.exe"
                };
                Process.Start(startInfo);
            };
        }
    }
}

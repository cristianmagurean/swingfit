﻿using Microsoft.Extensions.Logging;
using SwingFitApp.Constants;
using SwingFitApp.CQRS.UploadFile;
using SwingFitApp.Helpers;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

namespace SwingFitApp.LogManagement
{
    public class ProcessPendingLogs
    {
        private readonly string _serverUrl;
        private readonly string _gearsFolder;
        private readonly string _logsFolder;
        private readonly int _retryCount;
        private readonly int _retryFrequency;
        private readonly string _logFilter;      
        private readonly ILogger<UploadFileCommand> _loggerFileUploader;

        public ProcessPendingLogs(string serverUrl, string gearsFolder, string logsFolder, int retryCount, int retryFrequency, string logFilter, ILogger<UploadFileCommand> loggerFileUploader)
        {
            _serverUrl = serverUrl;
            _gearsFolder = gearsFolder;
            _logsFolder = logsFolder;
            _retryCount = retryCount;
            _retryFrequency = retryFrequency;
            _logFilter = logFilter;
          
            _loggerFileUploader = loggerFileUploader;
        }

        public async Task<ServerOperationStatus> Execute(BackgroundWorker backgroundWorker, string token, int pendingDays)
        {
            return await ProcessPendingFiles(backgroundWorker, token, pendingDays);
        }

        public ICollection<string> GetPendingFiles(int pendingDays)
        {
            List<string> pendingFileNames = new();
            var directoryInfo = new DirectoryInfo(_gearsFolder);
            ICollection<string> matchingFileNames = directoryInfo.GetFiles(_logFilter, SearchOption.AllDirectories)
              .AsEnumerable().Where(f => f.CreationTime > DateTime.Now.AddDays(0 - pendingDays))
              .Select(x => x.FullName)
              .ToList();

            foreach(var file in matchingFileNames)
            {
               if(!SearchLogFilePath(pendingDays, file))
               {
                    pendingFileNames.Add(file);                                      
               }
            }
            return pendingFileNames;   
        }

        private async Task<ServerOperationStatus> ProcessPendingFiles(BackgroundWorker backgroundWorker, string token, int pendingDays)
        {
            var fileUploader = new UploadFileCommand(_serverUrl, _retryCount, _retryFrequency, _loggerFileUploader);          
            foreach(var matchingFileName in GetPendingFiles(pendingDays))
            {
                if(backgroundWorker.CancellationPending)
                {
                    break;
                }
                var status = await fileUploader.ExecuteAsync(backgroundWorker, matchingFileName, token);
                if(status == ServerOperationStatus.Unauthorized)
                {
                    return ServerOperationStatus.Unauthorized;
                }
            }
            return ServerOperationStatus.Success;
        }

        private bool SearchLogFilePath(int pendingDays, string fileName)
        {
             for (int i = 0; i < pendingDays; i++)
            {
                string logFile = Path.Combine(_logsFolder, "info", $"log{DateTime.Today.AddDays(0 - i).ToString("yyyyMMdd")}.log");
                if (File.Exists(logFile))
                {
                    IEnumerable<string> logLines = FileHelper.WriteSafeReadLines(logFile);
                    if(logLines.Any(l => l.Split(' ')[5] == fileName))
                    {
                        return true;
                    }
                }
            }
            return false;
        }
    }
}

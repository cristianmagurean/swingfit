﻿using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace SwingFitApp.CQRS.ArchiveLogs
{
    public class ArchiveLogsCommand
    {
        private readonly string _logsFolder;
        private readonly string _archiveFolder;
        private readonly string _logFilter;
        private readonly int _days;
        private readonly ILogger<ArchiveLogsCommand> _logger;

        public ArchiveLogsCommand(string logsFolder, string archiveFolder, string logFilter, int days, ILogger<ArchiveLogsCommand> logger)
        {
            _logsFolder = logsFolder;
            _archiveFolder = archiveFolder;
            _logFilter = logFilter;
            _days = days;
            _logger = logger;
        }

        public void Execute()
        {
            Directory.CreateDirectory(_archiveFolder);

            Archive(_logsFolder);
            Archive($"{_logsFolder}/Errors");
            Archive($"{_logsFolder}/Info");
        }

        private void Archive(string folder)
        {
            Directory.CreateDirectory(folder);

            var directoryInfo = new DirectoryInfo(folder);
            ICollection<string> matchingFileNames = directoryInfo.GetFiles(_logFilter, SearchOption.TopDirectoryOnly)
              .AsEnumerable().Where(f => f.CreationTime < DateTime.Now.AddDays(0 - _days))
              .Select(x => x.FullName)
              .ToList();

            foreach (var file in matchingFileNames)
            {
                ArchiveFile(file, Path.Combine(_archiveFolder, Path.Combine(_archiveFolder, Path.GetFileName(file))));
            }
        }

        private void ArchiveFile(string sourceFile, string destFile)
        {
            try
            {
                _logger.LogInformation($"System action: Archive file {sourceFile}");
                File.Move(sourceFile, destFile, true);
            }
            catch (Exception ex)
            {
                _logger.LogError("Check server error: {message}", ex.Message);
            }
        }
    }
}

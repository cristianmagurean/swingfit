﻿using SwingFitApp.Properties;
using System;
using System.ComponentModel.DataAnnotations;
using System.IO;

namespace SwingFitApp.Configuration
{
    public class WorkerOptions
    {
        [Required]
        public static string GearsFolder
        {
            get
            {
                return Settings.Default.GearsFolder;
            }
            set
            {
                Settings.Default.GearsFolder = value;
            }
        }

        [Required]
        public static string ServerUrl
        {
            get
            {
                return Settings.Default.ServerUrl;
            }
            set
            {
                Settings.Default.ServerUrl = value;
            }
        }

        [Required]
        public static string LogsFolder
        {
            get
            {
                return Settings.Default.LogsFolder;
            }
            set
            {
                Settings.Default.LogsFolder = value;
            }
        }

        [Required]
        [Range(0, 100)]
        public static int RetryCount
        {
            get
            {
                return int.Parse(Settings.Default.RetryCount);
            }
            set
            {
                Settings.Default.RetryCount = value.ToString();
            }
        }

        [Required]
        [Range(1, 120)]
        public static int RetryFrequency
        {
            get
            {
                return int.Parse(Settings.Default.RetryFrequency);
            }
            set
            {
                Settings.Default.RetryFrequency = value.ToString();
            }
        }

        public static bool ValidateSettings()
        {
            bool result = Uri.TryCreate(ServerUrl, UriKind.Absolute, out Uri uriResult)
                && (uriResult.Scheme == Uri.UriSchemeHttp || uriResult.Scheme == Uri.UriSchemeHttps);

            if (!result || !Directory.Exists(GearsFolder) || !Directory.Exists(LogsFolder))
            {
                return false;
            }

            return true;
        }
    }
}

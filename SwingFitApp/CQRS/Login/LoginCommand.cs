﻿using Microsoft.Extensions.Logging;
using SwingFitApp.CQRS.Login.Dto;
using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Text.Json;
using System.Threading.Tasks;

namespace SwingFitApp.CQRS.Login
{
    internal class LoginCommand
    {
        private readonly string _serverUrl;
        private readonly ILogger<LoginCommand> _logger;

        public LoginCommand(string serverUrl, ILogger<LoginCommand> logger)
        {
            _serverUrl = serverUrl;
            _logger = logger;
        }

        public async Task<Token.Token> ExecuteAsync(string email, string key)
        {
            using (var httpClient = new HttpClient())
            {                
                httpClient.BaseAddress = new Uri(_serverUrl);
                var parameters = new Dictionary<string, string> {
                    { "mail", email },
                    { "key", key },
                    { "auth_method", "0" }
                };
                var encodedContent = new FormUrlEncodedContent(parameters);
                HttpResponseMessage response = null;
                try
                {
                    response = await httpClient.PostAsync("login", encodedContent);
                    response.EnsureSuccessStatusCode();
                    var responseContent = await response.Content.ReadAsStringAsync().ConfigureAwait(false);
                    var loginResponse = JsonSerializer.Deserialize<LoginResponseDto>(responseContent);
                    return new Token.Token
                    {
                        AuthToken = loginResponse.auth_token,
                        RefreshToken = loginResponse.refresh_token
                    };
                }
                catch (Exception ex)
                {                   
                    _logger.LogError("Login attempt with error: {message}", ex.Message);
                }
                finally
                {
                    response?.Dispose();
                }
            }  
            return null;
        }
    }
}

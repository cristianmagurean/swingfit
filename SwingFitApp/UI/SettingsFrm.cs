﻿using Microsoft.Extensions.Logging;
using SwingFitApp.Configuration;
using SwingFitApp.CQRS.CheckServerResponse;
using SwingFitApp.Properties;
using SwingFitApp.Token;
using System;
using System.Configuration;
using System.IO;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace SwingFitApp.UI
{
    public partial class SettingsFrm : Form
    {
        private readonly WorkerOptions _options;
        private readonly TokenManager _tokenManager;
        private readonly ILoggerFactory _loggerFactory;
        private readonly ILogger<SettingsFrm> _loggerSettingsFrm;
        private readonly ILogger<CheckServerResponseQuery> _loggerCheckServerResponseQuery;

        public SettingsFrm(WorkerOptions options, TokenManager tokenManager, ILoggerFactory loggerFactory)
        {
            InitializeComponent();
            txtServerURL.Text = WorkerOptions.ServerUrl;
            txtGearsFolder.Text = WorkerOptions.GearsFolder;
            txtLogsFolder.Text = WorkerOptions.LogsFolder;
            numericUpDownRetryCount.Value = WorkerOptions.RetryCount;
            numericUpDownRetryFrequency.Value = WorkerOptions.RetryFrequency;

            _options = options;
            _tokenManager = tokenManager;
            _loggerFactory = loggerFactory;
            _loggerSettingsFrm = loggerFactory.CreateLogger<SettingsFrm>();
            _loggerCheckServerResponseQuery = loggerFactory.CreateLogger<CheckServerResponseQuery>();
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btnOK_Click(object sender, EventArgs e)
        {
            if (ValidateChildren())
            {
                var frmLogin = new LoginFrm(txtServerURL.Text, _tokenManager, _loggerFactory);
                DialogResult dialogResult = frmLogin.ShowDialog();
                if (dialogResult == DialogResult.OK)
                {
                    SaveSettings();
                    this.Close();
                }
            }
        }

        private void SaveSettings()
        {
            WorkerOptions.ServerUrl = txtServerURL.Text;
            WorkerOptions.GearsFolder = txtGearsFolder.Text;
            WorkerOptions.LogsFolder = txtLogsFolder.Text;
            WorkerOptions.RetryCount = ((int)numericUpDownRetryCount.Value);
            WorkerOptions.RetryFrequency = ((int)numericUpDownRetryFrequency.Value);
            var path = ConfigurationManager.OpenExeConfiguration(ConfigurationUserLevel.PerUserRoamingAndLocal).FilePath;
            _loggerSettingsFrm.LogInformation($"Save settings at {path}");
            Settings.Default.Save();
        }

        private void txtServerURL_Validating(object sender, System.ComponentModel.CancelEventArgs e)
        {
            bool result = Uri.TryCreate(txtServerURL.Text, UriKind.Absolute, out Uri uriResult)
                && (uriResult.Scheme == Uri.UriSchemeHttp || uriResult.Scheme == Uri.UriSchemeHttps);
            if (!result)
            {
                errorProvider.SetError(txtServerURL, "Please enter a valid URL");
                e.Cancel = true;
            }
        }

        private void txtGearsFolder_Validating(object sender, System.ComponentModel.CancelEventArgs e)
        {
            if (!Directory.Exists(txtGearsFolder.Text))
            {
                errorProvider.SetError(txtGearsFolder, "Please enter a valid path");
                e.Cancel = true;
            }
        }

        private void txtLogsFolder_Validating(object sender, System.ComponentModel.CancelEventArgs e)
        {
            if (!Directory.Exists(txtLogsFolder.Text))
            {
                errorProvider.SetError(txtLogsFolder, "Please enter a valid path");
                e.Cancel = true;
            }
        }

        private void numericUpDownRetryCount_Validating(object sender, System.ComponentModel.CancelEventArgs e)
        {
            if ((numericUpDownRetryCount.Value < 1) || (numericUpDownRetryCount.Value > 100))
            {
                errorProvider.SetError(txtLogsFolder, "Please specify a range between 1 and 100");
                e.Cancel = true;
            }
        }

        private void numericUpDownRetryFrequency_Validating(object sender, System.ComponentModel.CancelEventArgs e)
        {
            if ((numericUpDownRetryFrequency.Value < 1) || (numericUpDownRetryFrequency.Value > 120))
            {
                errorProvider.SetError(txtLogsFolder, "Please specify a range between 1 and 120 minutes");
                e.Cancel = true;
            }
        }

        private void btnSelectGearsFolder_Click(object sender, EventArgs e)
        {
            folderBrowserDialog.RootFolder = Environment.SpecialFolder.ApplicationData;
            DialogResult result = folderBrowserDialog.ShowDialog();
            if (result == DialogResult.OK && !string.IsNullOrWhiteSpace(folderBrowserDialog.SelectedPath))
            {
                txtGearsFolder.Text = folderBrowserDialog.SelectedPath;
            }
        }

        private void btnSelectLogsFolder_Click(object sender, EventArgs e)
        {
            folderBrowserDialog.RootFolder = Environment.SpecialFolder.ApplicationData;
            DialogResult result = folderBrowserDialog.ShowDialog();
            if (result == DialogResult.OK && !string.IsNullOrWhiteSpace(folderBrowserDialog.SelectedPath))
            {
                txtLogsFolder.Text = folderBrowserDialog.SelectedPath;
            }
        }

        private bool ValidateServer()
        {
            var query = new CheckServerResponseQuery(txtServerURL.Text, _loggerCheckServerResponseQuery);
            var result = Task.Run(async () => await query.ExecuteAsync()).Result;
            if (!result)
            {
                errorProvider.SetError(txtServerURL, "Please enter the SwingFit server URL");
                return false;
            }
            return true;
        }
    }
}

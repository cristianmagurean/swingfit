﻿namespace SwingFitApp.UI
{
    partial class DashboardFrm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(DashboardFrm));
            this.tabControl = new System.Windows.Forms.TabControl();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.dataGridSyncedFiles = new System.Windows.Forms.DataGridView();
            this.FileName = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Timestamp = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.contextMenuStripGearLogs = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.listBoxSyncLogs = new System.Windows.Forms.ListBox();
            this.tabPage2 = new System.Windows.Forms.TabPage();
            this.dataGridActivityLines = new System.Windows.Forms.DataGridView();
            this.Activity = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.listBoxActivityLogs = new System.Windows.Forms.ListBox();
            this.btnUpgrade = new System.Windows.Forms.Button();
            this.lblUpgradeStatus = new System.Windows.Forms.Label();
            this.backgroundWorkerCheckForUpdates = new System.ComponentModel.BackgroundWorker();
            this.backgroundWorkerDownload = new System.ComponentModel.BackgroundWorker();
            this.btnRefresh = new System.Windows.Forms.Button();
            this.btnActions = new System.Windows.Forms.Button();
            this.contextMenuStripActions = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.tabControl.SuspendLayout();
            this.tabPage1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridSyncedFiles)).BeginInit();
            this.tabPage2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridActivityLines)).BeginInit();
            this.SuspendLayout();
            // 
            // tabControl
            // 
            this.tabControl.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tabControl.Controls.Add(this.tabPage1);
            this.tabControl.Controls.Add(this.tabPage2);
            this.tabControl.Location = new System.Drawing.Point(34, 46);
            this.tabControl.Margin = new System.Windows.Forms.Padding(2, 3, 2, 3);
            this.tabControl.Name = "tabControl";
            this.tabControl.SelectedIndex = 0;
            this.tabControl.Size = new System.Drawing.Size(1982, 940);
            this.tabControl.TabIndex = 0;
            // 
            // tabPage1
            // 
            this.tabPage1.Controls.Add(this.dataGridSyncedFiles);
            this.tabPage1.Controls.Add(this.listBoxSyncLogs);
            this.tabPage1.Location = new System.Drawing.Point(10, 58);
            this.tabPage1.Margin = new System.Windows.Forms.Padding(2, 3, 2, 3);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Padding = new System.Windows.Forms.Padding(2, 3, 2, 3);
            this.tabPage1.Size = new System.Drawing.Size(1962, 872);
            this.tabPage1.TabIndex = 0;
            this.tabPage1.Text = " Synced Files ";
            this.tabPage1.UseVisualStyleBackColor = true;
            // 
            // dataGridSyncedFiles
            // 
            this.dataGridSyncedFiles.AllowUserToAddRows = false;
            this.dataGridSyncedFiles.AllowUserToDeleteRows = false;
            this.dataGridSyncedFiles.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.dataGridSyncedFiles.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dataGridSyncedFiles.BackgroundColor = System.Drawing.Color.White;
            this.dataGridSyncedFiles.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridSyncedFiles.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.FileName,
            this.Timestamp});
            this.dataGridSyncedFiles.ContextMenuStrip = this.contextMenuStripGearLogs;
            this.dataGridSyncedFiles.Location = new System.Drawing.Point(325, 3);
            this.dataGridSyncedFiles.Margin = new System.Windows.Forms.Padding(2, 3, 2, 3);
            this.dataGridSyncedFiles.Name = "dataGridSyncedFiles";
            this.dataGridSyncedFiles.ReadOnly = true;
            this.dataGridSyncedFiles.RowHeadersVisible = false;
            this.dataGridSyncedFiles.RowHeadersWidth = 4;
            this.dataGridSyncedFiles.RowTemplate.Height = 49;
            this.dataGridSyncedFiles.Size = new System.Drawing.Size(1634, 858);
            this.dataGridSyncedFiles.TabIndex = 1;
            // 
            // FileName
            // 
            this.FileName.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.FileName.HeaderText = "File name";
            this.FileName.MinimumWidth = 12;
            this.FileName.Name = "FileName";
            this.FileName.ReadOnly = true;
            // 
            // Timestamp
            // 
            this.Timestamp.HeaderText = "Timestamp";
            this.Timestamp.MinimumWidth = 12;
            this.Timestamp.Name = "Timestamp";
            this.Timestamp.ReadOnly = true;
            // 
            // contextMenuStripGearLogs
            // 
            this.contextMenuStripGearLogs.ImageScalingSize = new System.Drawing.Size(40, 40);
            this.contextMenuStripGearLogs.Name = "contextMenuStripGearLogs";
            this.contextMenuStripGearLogs.Size = new System.Drawing.Size(61, 4);
            // 
            // listBoxSyncLogs
            // 
            this.listBoxSyncLogs.Dock = System.Windows.Forms.DockStyle.Left;
            this.listBoxSyncLogs.FormattingEnabled = true;
            this.listBoxSyncLogs.ItemHeight = 41;
            this.listBoxSyncLogs.Location = new System.Drawing.Point(2, 3);
            this.listBoxSyncLogs.Margin = new System.Windows.Forms.Padding(2, 3, 2, 3);
            this.listBoxSyncLogs.Name = "listBoxSyncLogs";
            this.listBoxSyncLogs.Size = new System.Drawing.Size(339, 866);
            this.listBoxSyncLogs.TabIndex = 0;
            this.listBoxSyncLogs.SelectedIndexChanged += new System.EventHandler(this.listBoxSyncLogs_SelectedIndexChanged);
            // 
            // tabPage2
            // 
            this.tabPage2.Controls.Add(this.dataGridActivityLines);
            this.tabPage2.Controls.Add(this.listBoxActivityLogs);
            this.tabPage2.Location = new System.Drawing.Point(10, 58);
            this.tabPage2.Margin = new System.Windows.Forms.Padding(2, 3, 2, 3);
            this.tabPage2.Name = "tabPage2";
            this.tabPage2.Padding = new System.Windows.Forms.Padding(2, 3, 2, 3);
            this.tabPage2.Size = new System.Drawing.Size(1962, 872);
            this.tabPage2.TabIndex = 1;
            this.tabPage2.Text = " Activity Log ";
            this.tabPage2.UseVisualStyleBackColor = true;
            // 
            // dataGridActivityLines
            // 
            this.dataGridActivityLines.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.dataGridActivityLines.BackgroundColor = System.Drawing.Color.White;
            this.dataGridActivityLines.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridActivityLines.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.Activity});
            this.dataGridActivityLines.Location = new System.Drawing.Point(316, 3);
            this.dataGridActivityLines.Margin = new System.Windows.Forms.Padding(2, 3, 2, 3);
            this.dataGridActivityLines.Name = "dataGridActivityLines";
            this.dataGridActivityLines.RowHeadersVisible = false;
            this.dataGridActivityLines.RowHeadersWidth = 4;
            this.dataGridActivityLines.RowTemplate.Height = 49;
            this.dataGridActivityLines.Size = new System.Drawing.Size(1644, 858);
            this.dataGridActivityLines.TabIndex = 1;
            // 
            // Activity
            // 
            this.Activity.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.Activity.HeaderText = "Activity";
            this.Activity.MinimumWidth = 12;
            this.Activity.Name = "Activity";
            // 
            // listBoxActivityLogs
            // 
            this.listBoxActivityLogs.Dock = System.Windows.Forms.DockStyle.Left;
            this.listBoxActivityLogs.FormattingEnabled = true;
            this.listBoxActivityLogs.ItemHeight = 41;
            this.listBoxActivityLogs.Location = new System.Drawing.Point(2, 3);
            this.listBoxActivityLogs.Margin = new System.Windows.Forms.Padding(2, 3, 2, 3);
            this.listBoxActivityLogs.Name = "listBoxActivityLogs";
            this.listBoxActivityLogs.Size = new System.Drawing.Size(315, 866);
            this.listBoxActivityLogs.TabIndex = 0;
            this.listBoxActivityLogs.SelectedIndexChanged += new System.EventHandler(this.listBoxActivityLogs_SelectedIndexChanged);
            // 
            // btnUpgrade
            // 
            this.btnUpgrade.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnUpgrade.Enabled = false;
            this.btnUpgrade.Location = new System.Drawing.Point(1829, 989);
            this.btnUpgrade.Margin = new System.Windows.Forms.Padding(2, 3, 2, 3);
            this.btnUpgrade.Name = "btnUpgrade";
            this.btnUpgrade.Size = new System.Drawing.Size(187, 57);
            this.btnUpgrade.TabIndex = 1;
            this.btnUpgrade.Text = "Upgrade";
            this.btnUpgrade.UseVisualStyleBackColor = true;
            this.btnUpgrade.Click += new System.EventHandler(this.btnUpgrade_Click);
            // 
            // lblUpgradeStatus
            // 
            this.lblUpgradeStatus.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.lblUpgradeStatus.AutoSize = true;
            this.lblUpgradeStatus.Location = new System.Drawing.Point(46, 989);
            this.lblUpgradeStatus.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.lblUpgradeStatus.Name = "lblUpgradeStatus";
            this.lblUpgradeStatus.Size = new System.Drawing.Size(469, 41);
            this.lblUpgradeStatus.TabIndex = 2;
            this.lblUpgradeStatus.Text = "The installed version is up to date.";
            // 
            // backgroundWorkerCheckForUpdates
            // 
            this.backgroundWorkerCheckForUpdates.DoWork += new System.ComponentModel.DoWorkEventHandler(this.backgroundWorkerCheckForUpdates_DoWork);
            this.backgroundWorkerCheckForUpdates.RunWorkerCompleted += new System.ComponentModel.RunWorkerCompletedEventHandler(this.backgroundWorkerCheckForUpdates_RunWorkerCompleted);
            // 
            // backgroundWorkerDownload
            // 
            this.backgroundWorkerDownload.DoWork += new System.ComponentModel.DoWorkEventHandler(this.backgroundWorkerDownload_DoWork);
            this.backgroundWorkerDownload.RunWorkerCompleted += new System.ComponentModel.RunWorkerCompletedEventHandler(this.backgroundWorkerDownload_RunWorkerCompleted);
            // 
            // btnRefresh
            // 
            this.btnRefresh.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnRefresh.Location = new System.Drawing.Point(1610, 25);
            this.btnRefresh.Margin = new System.Windows.Forms.Padding(2, 3, 2, 3);
            this.btnRefresh.Name = "btnRefresh";
            this.btnRefresh.Size = new System.Drawing.Size(187, 57);
            this.btnRefresh.TabIndex = 6;
            this.btnRefresh.Text = "Refresh";
            this.btnRefresh.UseVisualStyleBackColor = true;
            this.btnRefresh.Click += new System.EventHandler(this.btnRefresh_Click);
            // 
            // btnActions
            // 
            this.btnActions.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnActions.Location = new System.Drawing.Point(1819, 25);
            this.btnActions.Margin = new System.Windows.Forms.Padding(2, 3, 2, 3);
            this.btnActions.Name = "btnActions";
            this.btnActions.Size = new System.Drawing.Size(187, 57);
            this.btnActions.TabIndex = 8;
            this.btnActions.Text = "Actions";
            this.btnActions.UseVisualStyleBackColor = true;
            this.btnActions.Click += new System.EventHandler(this.btnActions_Click);
            // 
            // contextMenuStripActions
            // 
            this.contextMenuStripActions.ImageScalingSize = new System.Drawing.Size(40, 40);
            this.contextMenuStripActions.Name = "contextMenuStripActions";
            this.contextMenuStripActions.Size = new System.Drawing.Size(361, 59);
            // 
            // DashboardFrm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(17F, 41F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.ClientSize = new System.Drawing.Size(2050, 1074);
            this.Controls.Add(this.btnActions);
            this.Controls.Add(this.btnRefresh);
            this.Controls.Add(this.lblUpgradeStatus);
            this.Controls.Add(this.btnUpgrade);
            this.Controls.Add(this.tabControl);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Margin = new System.Windows.Forms.Padding(2, 3, 2, 3);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "DashboardFrm";
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "SwingFit App";
            this.Load += new System.EventHandler(this.DashboardFrm_Load);
            this.tabControl.ResumeLayout(false);
            this.tabPage1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridSyncedFiles)).EndInit();
            this.tabPage2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridActivityLines)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TabControl tabControl;
        private System.Windows.Forms.TabPage tabPage1;
        private System.Windows.Forms.TabPage tabPage2;
        private System.Windows.Forms.Button btnUpgrade;
        private System.Windows.Forms.Label lblUpgradeStatus;
        private System.ComponentModel.BackgroundWorker backgroundWorkerCheckForUpdates;
        private System.ComponentModel.BackgroundWorker backgroundWorkerDownload;
        private System.Windows.Forms.ListBox listBoxActivityLogs;
        private System.Windows.Forms.DataGridView dataGridActivityLines;
        private System.Windows.Forms.DataGridViewTextBoxColumn Activity;
        private System.Windows.Forms.ListBox listBoxSyncLogs;
        private System.Windows.Forms.DataGridView dataGridSyncedFiles;
        private System.Windows.Forms.DataGridViewTextBoxColumn FileName;
        private System.Windows.Forms.DataGridViewTextBoxColumn Timestamp;
        private System.Windows.Forms.Button btnRefresh;
        private System.Windows.Forms.Button btnActions;
        private System.Windows.Forms.ContextMenuStrip contextMenuStripActions;
        private System.Windows.Forms.ContextMenuStrip contextMenuStripGearLogs;
    }
}
﻿using Microsoft.Extensions.Logging;
using SwingFitApp.Constants;
using SwingFitApp.Helpers;
using System;
using System.IO;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;

namespace SwingFitApp.CQRS.UploadLogs
{
    public class UploadLogsCommand
    {
        private readonly string _serverUrl;
        private readonly ILogger<UploadLogsCommand> _logger;

        public UploadLogsCommand(string serverUrl, ILogger<UploadLogsCommand> logger)
        {
            _serverUrl = serverUrl;
            _logger = logger;
        }

        public async Task<ServerOperationStatus> ExecuteAsync(string filePath, string token)
        {
            using (var httpClient = new HttpClient())
            {
                HttpResponseMessage response = null;

                try
                {
                    _logger.LogDebug("Attempt to upload file {filePath}", filePath);
                    httpClient.BaseAddress = new Uri(_serverUrl);
                    httpClient.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("*/*"));
                    httpClient.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", token);

                    var requestContent = new MultipartFormDataContent();
                    var byteArray = FileHelper.WriteSafeReadByteArray(filePath);
                    var fileContent = new ByteArrayContent(byteArray);
                    fileContent.Headers.ContentType = new MediaTypeHeaderValue("multipart/form-data");
                    requestContent.Add(fileContent, "file", Path.GetFileName(filePath).Replace(".log", ".txt"));

                    response = await httpClient.PostAsync("upload_logs", requestContent);
                    if (response.IsSuccessStatusCode)
                    {
                        _logger.LogInformation("File {filePath} uploaded with success", filePath);
                        return ServerOperationStatus.Success;
                    }
                    else
                    {
                        _logger.LogError("File {filePath} upload error: {status} {reason}", filePath, response.StatusCode, response.ReasonPhrase);
                        if (response.StatusCode == HttpStatusCode.Unauthorized)
                        {
                            return ServerOperationStatus.Unauthorized;
                        }
                    }
                }
                catch (Exception ex)
                {
                    _logger.LogError("File {filePath} upload error: {message}", filePath, ex.Message);
                }
                finally
                {
                    response?.Dispose();
                }
                return ServerOperationStatus.Error;
            }
        }
    }
}

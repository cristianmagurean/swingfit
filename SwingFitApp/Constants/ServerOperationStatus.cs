﻿namespace SwingFitApp.Constants
{
    public enum ServerOperationStatus
    {
        Success,
        Unauthorized,
        Error
    }
}

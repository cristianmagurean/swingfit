﻿Steps to generate a new installer
1. Increment AssemblyVersion and FileVersion in SwingfitApp.csproj file
2. Select 'Release' configuration
3. RebuilBuild All for SwingFitApp project
4. Select SwingFitAppInstaller project properties window and increment version to be identical with the project one 
5. ReBuild All for SwingFitAppInstaller
6. Copy SwingFitAppInstaller.msi from SwingFitAppInstaller release folder https://drive.google.com/drive/u/1/folders/1vi4gRf71-c5sozUx_LE4HnV5MF5X8GVU


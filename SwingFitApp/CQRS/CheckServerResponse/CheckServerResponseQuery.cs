﻿using Microsoft.Extensions.Logging;
using System;
using System.Net.Http;
using System.Threading.Tasks;

namespace SwingFitApp.CQRS.CheckServerResponse
{
    internal class CheckServerResponseQuery
    {
        private readonly string _serverUrl;
        private readonly ILogger<CheckServerResponseQuery> _logger;

        public CheckServerResponseQuery(string serverUrl, ILogger<CheckServerResponseQuery> logger)
        {
            _serverUrl = serverUrl;
            _logger = logger;
        }

        public async Task<bool> ExecuteAsync()
        {
            using (var httpClient = new HttpClient())
            {
                HttpResponseMessage response = null;
                try
                {
                    httpClient.Timeout = TimeSpan.FromSeconds(5);
                    response = await httpClient.GetAsync($"{_serverUrl}/settings");
                    response.EnsureSuccessStatusCode();
                }
                catch (Exception ex)
                {
                    _logger.LogError("Check server error: {message}", ex.Message);
                    return false;
                }
                finally
                {
                    response?.Dispose();
                }

            }
            return true;
        }
    }
}
